# install node
```bash
sudo apt install curl
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

# Docker und Zeugs
```bash
# install Docker
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
    
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
sudo apt-get update

sudo apt-get install docker-ce

# group might already exist
sudo groupadd docker

sudo usermod -aG docker $USER

# log off / on afterwards

https://docs.docker.com/engine/installation/linux/linux-postinstall/#manage-docker-as-a-non-root-user

sudo systemctl enable docker

# fix cgroupdriver error
# To fix this you have to add “–exec-opt native.cgroupdriver=systemd” to ExecStart of docker. 
# The best way to do this is to add a addin file 
# /etc/systemd/system/docker.service.d/override.conf with following content:

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo nano /etc/systemd/system/docker.service.d/override.conf

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// --exec-opt native.cgroupdriver=systemd

# then restart
sudo systemctl daemon-reload
sudo systemctl restart docker

# OpenShift
```bash

```

## OC Client Tools
```bash
wget https://github.com/openshift/origin/releases/download/v3.7.0/openshift-origin-client-tools-v3.7.0-7ed6862-linux-64bit.tar.gz
tar xvzf openshift-origin-client-tools-v3.7.0-7ed6862-linux-64bit.tar.gz
echo $PATH
# mv oc to ~/.local/bin, check if folders exists
sudo mv oc ...

# load Gitlab Omnibus template 
# following this:
# https://about.gitlab.com/2016/06/28/get-started-with-openshift-origin-3-and-gitlab/
# but 
wget https://gitlab.com/gitlab-org/omnibus-gitlab/raw/master/docker/openshift-template.json
oc create -f openshift-template.json -n openshift

# see https://github.com/openshift-evangelists/oc-cluster-wrapper
# persist data, set config, ...
oc cluster up --skip-registry-check=true

